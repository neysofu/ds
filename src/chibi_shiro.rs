//! The interactive parts of DS.  Relies on the game module.

extern crate ggez;

use ggez::event::{self, KeyCode, KeyMods, MouseButton};
use ggez::graphics::{self, Color, DrawParam, Mesh};
use ggez::{Context, GameResult};

use game::border::Border;
use game::dwarf::Population;
use game::font::Font;
use game::grid::Grid;
use game::layer::Layer;
use game::location::Direction;
use game::location::Location;
use game::point::Point;
use game::symbol::{Background, TextCache};
use game::tile::Tile;

const DESIRED_FPS: u32 = 60;

pub enum Event {
    Continue,
    Quit,
    Resize(f32, f32),
    Walk(Direction),
    Warp(f32, f32),
    PlaceDwarf,
    PlaceExit,
    Zoom(f32),
}

pub struct MainState {
    text_cache: TextCache,
    dirty: bool,
    event: Event,
    border: Border,
    frames: usize,
    grid: Grid,
    grid_mesh: Mesh,
    background: Background,
    layer: Layer,
    location: Location,
    population: Population,
}

fn seconds(frames: u32, fps: u32) -> f64 {
    f64::from(frames) / f64::from(fps)
}

fn load_font(ctx: &mut Context, path: &str) -> Option<graphics::Font> {
    if let Ok(font) = graphics::Font::new(ctx, path) {
        Some(font)
    } else {
        None
    }
}

impl MainState {
    pub fn new(ctx: &mut Context) -> GameResult<MainState> {
        let regular = load_font(ctx, "/FiraCode-Regular.ttf");
        let bold = load_font(ctx, "/FiraCode-Bold.ttf");
        let font = Font {
            regular,
            bold,
            size: 26.0,
        };
        let coords = graphics::screen_coordinates(ctx);
        let border = Border::new(font, Point::new(0.0, 0.0), coords.w, coords.h);
        let grid = Grid::new(border.grid_size(), font, 1.2);
        let mut layer = Layer::new(20, 20, "seed");
        layer.create_map_contents();
        let grid_mesh = grid.mesh(ctx, Color::new(0.0, 0.4, 0.6, 1.0))?;

        let s = MainState {
            text_cache: TextCache::default(),
            background: Background::new(ctx, grid.tile_size() as u16).unwrap(),
            dirty: true,
            event: Event::Continue,
            border,
            frames: 0,
            grid,
            grid_mesh,
            layer,
            location: Location::new(0, 0),
            population: Default::default(),
        };
        Ok(s)
    }

    pub fn resize(&mut self, ctx: &mut Context, width: f32, height: f32) -> GameResult<bool> {
        graphics::set_screen_coordinates(ctx, graphics::Rect::new(0.0, 0.0, width, height))?;
        self.border = Border::new(self.grid.font, Point::new(0.0, 0.0), width, height);

        // You have to present after setting the screen coordinates otherwise the
        // mesh will draw improperly the first time.
        graphics::present(ctx)?;

        self.grid = self.grid.resize(self.border.grid_size());
        self.grid_mesh = self.grid.mesh(ctx, Color::new(0.0, 0.4, 0.6, 1.0))?;

        Ok(true)
    }

    fn valid_zoom(new_size: f32, old_size: f32) -> bool {
        new_size >= 5.0 && new_size <= 15000.0 && (new_size - old_size).abs() > std::f32::EPSILON
    }

    pub fn zoom(&mut self, ctx: &mut Context, amount: f32) -> GameResult<bool> {
        let mut updated = false;
        let new_size = self.grid.font.size + (amount / 0.2).trunc();
        if Self::valid_zoom(new_size, self.grid.font.size) {
            self.grid = self.grid.change_font_size(new_size);
            self.text_cache = TextCache::default();
            self.background = Background::new(ctx, self.grid.tile_size() as u16).unwrap();
            self.grid_mesh = self.grid.mesh(ctx, Color::new(0.0, 0.4, 0.6, 1.0))?;
            updated = true;
        }
        Ok(updated)
    }

    pub fn walk(&mut self, dir: Direction) -> GameResult<(bool)> {
        let mut updated = false;

        if let Some(new_location) = self.layer.try_walk(&self.location, dir) {
            self.location = new_location;
            updated = true;
        }

        Ok(updated)
    }

    pub fn warp(&mut self, x: f32, y: f32) -> GameResult<(bool)> {
        let mut updated = false;

        // Add the grid_offset for the point when determining the new grid location
        if let Some(location) = self
            .grid
            .location(Point::new(x, y))
            .and_then(|grid_location| self.grid.center_offset(grid_location))
            .and_then(|offset| self.layer.try_warp(&self.location, offset))
        {
            self.location = location;
            updated = true;
        }

        Ok(updated)
    }

    pub fn place_dwarf(&mut self) -> GameResult<(bool)> {
        Ok(self.population.add_dwarf(self.location, &mut self.layer))
    }

    pub fn place_exit(&mut self) -> GameResult<(bool)> {
        Ok(self.layer.place_exit(self.location))
    }

    pub fn handle_events(&mut self, ctx: &mut Context) -> GameResult<(bool)> {
        let mut updated = false;

        match self.event {
            Event::Resize(width, height) => {
                updated = self.resize(ctx, width, height)?;
            }
            Event::Zoom(amount) => {
                updated = self.zoom(ctx, amount)?;
            }
            Event::Quit => {
                ggez::event::quit(ctx);
            }
            Event::Walk(dir) => {
                updated = self.walk(dir)?;
            }
            Event::Warp(x, y) => {
                updated = self.warp(x, y)?;
            }
            Event::PlaceDwarf => {
                updated = self.place_dwarf()?;
            }
            Event::PlaceExit => {
                updated = self.place_exit()?;
            }

            _ => (),
        }
        self.event = Event::Continue;
        Ok(updated)
    }

    fn update_frame_count(&mut self, ctx: &mut Context) {
        while ggez::timer::check_update_time(ctx, DESIRED_FPS) {
            self.frames += 1;
        }
    }

    // Sleep for the remainder of the frame
    fn sleep(&mut self) {
        let duration = ggez::timer::f64_to_duration(seconds(1, DESIRED_FPS));
        ggez::timer::sleep(duration);
    }

    fn update_border(&mut self, ctx: &mut Context) -> bool {
        if (seconds(self.frames as u32, DESIRED_FPS)) > 3.0 {
            self.border.update_fps(ggez::timer::fps(ctx));
            self.frames = 0;
            true
        } else {
            false
        }
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        let mut updated = false;

        self.update_frame_count(ctx);
        updated |= self.handle_events(ctx)?;
        updated |= self.update_border(ctx);

        self.dirty = updated;
        if !self.dirty {
            self.sleep();
            self.dirty = self.population.tick(&self.layer);
            if self.dirty {
                self.population.update_layer(&mut self.layer);
            }
        }
        Ok(())
    }

    fn resize_event(&mut self, _ctx: &mut Context, width: f32, height: f32) {
        self.event = Event::Resize(width, height);
    }

    fn mouse_wheel_event(&mut self, _ctx: &mut Context, _x: f32, y: f32) {
        if y != 0.0 {
            self.event = Event::Zoom(y);
        }
    }

    fn mouse_button_up_event(&mut self, _ctx: &mut Context, button: MouseButton, x: f32, y: f32) {
        if button == event::MouseButton::Left {
            self.event = Event::Warp(x, y);
        }
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: KeyCode, _keymod: KeyMods) {
        // Quit
        if keycode == event::KeyCode::Q {
            self.event = Event::Quit;
        }

        // Movement
        if keycode == event::KeyCode::J {
            self.event = Event::Walk(Direction::Down);
        }
        if keycode == event::KeyCode::B {
            self.event = Event::Walk(Direction::DownLeft);
        }
        if keycode == event::KeyCode::N {
            self.event = Event::Walk(Direction::DownRight);
        }
        if keycode == event::KeyCode::K {
            self.event = Event::Walk(Direction::Up);
        }
        if keycode == event::KeyCode::Y {
            self.event = Event::Walk(Direction::UpLeft);
        }
        if keycode == event::KeyCode::U {
            self.event = Event::Walk(Direction::UpRight);
        }
        if keycode == event::KeyCode::H {
            self.event = Event::Walk(Direction::Left);
        }
        if keycode == event::KeyCode::L {
            self.event = Event::Walk(Direction::Right);
        }

        // Place dwarf
        if keycode == event::KeyCode::D {
            self.event = Event::PlaceDwarf;
        }

        // Place Exit
        if keycode == event::KeyCode::X {
            self.event = Event::PlaceExit;
        }
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        if self.dirty {
            graphics::clear(ctx, Color::new(0.0, 0.0, 0.0, 1.0));

            self.border.draw(ctx)?;

            let mut background_batch = self.background.new_batch();
            self.grid.queue_tiles(
                ctx,
                &self.layer,
                &mut self.text_cache,
                &mut background_batch,
                self.location,
                self.grid.destination(),
            )?;
            Tile::draw_queued(ctx, &background_batch)?;

            graphics::draw(
                ctx,
                &self.grid_mesh,
                DrawParam::default().dest(self.grid.destination()),
            )?;

            graphics::present(ctx)?;
        }

        Ok(())
    }
}
