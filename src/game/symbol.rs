//! Code around symbols to be plotted on the map.
//! A symbol is composed of a string indicating what to draw,
//! a weight (either bold or regular), a colour, and a background
//! colour.
use super::font::Font;
use super::point::Point;
use super::rand::Rng as RngTrait;
use super::rng::Rng;
use ggez::graphics::spritebatch::SpriteBatch;
use ggez::graphics::{Align, Color, Image, Scale, Text, TextFragment};
use ggez::{Context, GameResult};
use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::convert::TryInto;
use std::default::Default;
use std::hash::{Hash, Hasher};

/// The font weight to use when drawing the `Symbol` string.
#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum Weight {
    Regular,
    Bold,
}

pub struct Background(Image);

impl Background {
    pub fn new(ctx: &mut Context, size: u16) -> GameResult<Self> {
        let bytesize = u64::from(size) * u64::from(size) * 4;
        let rgba = vec![255_u8; bytesize.try_into().unwrap()];
        Image::from_rgba8(ctx, size, size, &rgba).map(Self)
    }

    pub fn new_batch(&self) -> SpriteBatch {
        SpriteBatch::new(self.0.clone())
    }
}

/// Represents something to be plotted on the map.
/// The `Symbol` is composed of a string that says
/// what should be drawn, the colour to draw it,
/// the font weight to draw it with and the background
/// colour to draw it agains.  The string and background
/// are optional.  If they are `None` then nothing will
/// be drawn when the symbol is rendered.
#[derive(PartialEq, Debug)]
pub struct Symbol<'a> {
    pub string: Option<&'a str>,
    pub color: Color,
    pub weight: Weight,
    pub background: Option<Color>,
}

impl<'a> Default for Symbol<'a> {
    /// Default symbol when drawing nothing.
    fn default() -> Self {
        Symbol {
            string: None,
            color: Color::new(1.0, 1.0, 1.0, 1.0),
            weight: Weight::Regular,
            background: None,
        }
    }
}

impl<'a> Symbol<'a> {
    /// Returns a `Text` rendering the string in the correct font and colour.
    /// Returns `None` if string is `None`.
    pub fn text(&self, font: Font, bounds: f32) -> Option<Text> {
        let used_font = match self.weight {
            Weight::Regular => font.regular,
            Weight::Bold => font.bold,
        };

        self.string.map(|string| {
            let mut text = Text::new(TextFragment {
                text: string.to_string(),
                color: Some(self.color),
                font: used_font,
                scale: Some(Scale::uniform(font.size)),
            });
            text.set_bounds(Point::new(bounds, bounds), Align::Center);
            text
        })
    }

    /// Returns a tag to use when caching the foreground of the `Symbol`.
    pub fn fg_hash(&self) -> Option<u64> {
        let mut hasher = DefaultHasher::new();
        self.string?.hash(&mut hasher);
        self.weight.hash(&mut hasher);
        hasher.write_u32(self.color.to_rgb_u32());
        Some(hasher.finish())
    }

    /// Returns a tag to use when caching the background of the `Symbol`.
    pub fn bg_hash(&self) -> Option<u64> {
        let mut hasher = DefaultHasher::new();
        let hash_value = self.background?.to_rgb_u32();
        hasher.write_u32(hash_value);
        Some(hasher.finish())
    }

    pub fn error() -> Self {
        Symbol {
            string: Some("?"),
            color: Color::new(1.0, 0.3, 0.3, 1.0),
            weight: Weight::Regular,
            background: Some(Color::new(0.5, 0.5, 0.5, 1.0)),
        }
    }

    pub fn off_map() -> Self {
        Symbol {
            string: None,
            color: Color::new(0.5, 0.5, 0.5, 1.0),
            weight: Weight::Regular,
            background: Some(Color::new(1.0, 0.9, 0.7, 1.0)),
        }
    }

    pub fn cursor() -> Self {
        Symbol {
            string: Some("X"),
            color: Color::new(1.0, 1.0, 0.0, 1.0),
            weight: Weight::Bold,
            ..Default::default()
        }
    }

    fn generate_terrain(terrains: &[&'a str], terrain_index: usize, color: Color) -> Self {
        if terrain_index > 0 && terrain_index < terrains.len() + 1 {
            let string = terrains[terrain_index - 1];

            Symbol {
                string: Some(string),
                color,
                ..Default::default()
            }
        } else {
            Symbol {
                ..Default::default()
            }
        }
    }

    // Only 5% of the terrain is "intersting" (uses a non-default symbol)
    fn terrain_type(terrains: &[&str], mut rng: Rng) -> usize {
        if rng.gen_range(1, 21) == 1 {
            rng.gen_range(1, terrains.len() + 1)
        } else {
            0
        }
    }

    pub fn undiscovered_terrain(rng: Rng) -> Self {
        const TERRAINS: [&str; 5] = ["'", ",", ".", "`", "%"];
        let terrain_index = Symbol::terrain_type(&TERRAINS, rng);
        let color = Color::new(0.5, 0.5, 0.5, 1.0);
        Symbol::generate_terrain(&TERRAINS, terrain_index, color)
    }

    pub fn floor(mut rng: Rng) -> Self {
        const TERRAINS: [&str; 4] = ["'", ",", ".", "`"];
        let terrain_index = rng.gen_range(1, TERRAINS.len() + 1);
        let color = Color::new(0.8, 0.8, 0.2, 1.0);
        Symbol::generate_terrain(&TERRAINS, terrain_index, color)
    }

    pub fn wall() -> Self {
        Symbol {
            string: Some("#"),
            color: Color::new(1.0, 1.0, 1.0, 1.0),
            background: Some(Color::new(0.7, 0.7, 0.7, 1.0)),
            weight: Weight::Bold,
        }
    }

    pub fn dwarf() -> Self {
        Symbol {
            string: Some("☺"),
            color: Color::new(1.0, 0.0, 1.0, 1.0),
            ..Default::default()
        }
    }

    pub fn exit() -> Self {
        Symbol {
            string: Some("▓"),
            color: Color::new(1.0, 0.0, 1.0, 1.0),
            ..Default::default()
        }
    }
}

#[derive(Default)]
pub struct TextCache(HashMap<Option<u64>, Option<Text>>);
impl TextCache {
    pub fn get(&mut self, symbol: &Symbol, font: Font, bounds: f32) -> &Option<Text> {
        self.0
            .entry(symbol.fg_hash())
            .or_insert_with(|| symbol.text(font, bounds))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_terrain() {
        let terrains = vec!["'", ",", ".", "`", "%"];
        let color = Color::new(0.5, 0.5, 0.5, 1.0);

        // Zero means that you want default rock
        assert_eq!(
            Symbol::generate_terrain(&terrains, 0, color),
            Symbol {
                ..Default::default()
            }
        );

        // Anything more than zero is interesting rock
        terrains.iter().enumerate().for_each(|(i, terrain)| {
            assert_eq!(
                Symbol::generate_terrain(&terrains, i + 1, color),
                Symbol {
                    string: Some(terrain),
                    color: color,
                    ..Default::default()
                }
            )
        });

        // Anything outside the range will generate an default tile
        assert_eq!(
            Symbol::generate_terrain(&terrains, 100, color),
            Symbol {
                ..Default::default()
            }
        )
    }

    #[test]
    fn test_undiscovered_terrain() {
        let seed = "test_seed";
        let rng = Rng::new(seed);
        assert_eq!(
            Symbol::undiscovered_terrain(rng),
            Symbol {
                ..Default::default()
            }
        );
    }

    #[test]
    fn test_floor() {
        let seed = "test_seed";
        let rng = Rng::new(seed);
        assert_eq!(
            Symbol::floor(rng),
            Symbol {
                string: Some("`"),
                color: Color::new(0.8, 0.8, 0.2, 1.0),
                ..Default::default()
            }
        );
    }
}
