//! Code for dealing with points of the graphics screen.
use ggez::mint::Point2;
use std::convert::From;
use std::ops::Add;
use std::ops::Sub;

/// A location on the graphics screen in pixels.
/// Basically a wrapper around the points that ggez uses.
#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Point(Point2<f32>);

impl From<Point2<f32>> for Point {
    /// Convert from an nalgebra `Point2` (which ggez uses) to our `Point` wrapper.
    fn from(p: Point2<f32>) -> Self {
        Self(p)
    }
}

impl From<Point> for Point2<f32> {
    /// Convert from our `Point` wrapper to the points that ggez uses.
    fn from(p: Point) -> Self {
        p.0
    }
}

impl Point {
    /// Create a new `Point` describing a location on the screen in pixels.
    pub fn new(x: f32, y: f32) -> Self {
        Self(Point2 { x, y })
    }

    pub fn x(self) -> f32 {
        self.0.x
    }

    pub fn y(self) -> f32 {
        self.0.y
    }
}

impl Add for Point {
    type Output = Point;

    /// Add 2 points together.
    fn add(self, other: Point) -> Point {
        Point::new(self.0.x + other.0.x, self.0.y + other.0.y)
    }
}

impl Sub for Point {
    type Output = Point;

    /// subtract 2 points from each other.
    fn sub(self, other: Point) -> Point {
        Point::new(self.0.x - other.0.x, self.0.y - other.0.y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_points() {
        let a = Point::new(1.0, 2.0);
        let b = Point::new(4.0, 6.0);

        assert_eq!(a.add(b), Point::new(5.0, 8.0));
    }

    #[test]
    fn plus_points() {
        let a = Point::new(1.0, 2.0);
        let b = Point::new(4.0, 6.0);

        assert_eq!(a + b, Point::new(5.0, 8.0));
    }

    #[test]
    fn sub_points() {
        let a = Point::new(1.0, 2.0);
        let b = Point::new(4.0, 6.0);

        assert_eq!(a.sub(b), Point::new(-3.0, -4.0));
    }

    #[test]
    fn minus_points() {
        let a = Point::new(1.0, 2.0);
        let b = Point::new(4.0, 6.0);

        assert_eq!(a - b, Point::new(-3.0, -4.0));
    }
}
