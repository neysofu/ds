//! Code for dealing with rectangles on the screen
use super::point::Point;
use ggez::graphics;
use std::convert::From;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Rect(graphics::Rect);

impl From<graphics::Rect> for Rect {
    fn from(r: graphics::Rect) -> Self {
        Self(r)
    }
}

impl From<Rect> for graphics::Rect {
    fn from(r: Rect) -> Self {
        r.0
    }
}

impl Rect {
    pub fn new(x: f32, y: f32, w: f32, h: f32) -> Self {
        Self(graphics::Rect { x, y, w, h })
    }

    pub fn destination(&self) -> Point {
        Point::new(self.left(), self.top())
    }

    pub fn width(&self) -> f32 {
        self.right() - self.left()
    }

    pub fn height(&self) -> f32 {
        self.bottom() - self.top()
    }

    pub fn left(&self) -> f32 {
        self.0.left()
    }

    pub fn right(&self) -> f32 {
        self.0.right()
    }

    pub fn top(&self) -> f32 {
        self.0.top()
    }

    pub fn bottom(&self) -> f32 {
        self.0.bottom()
    }

    pub fn shrink_by(&self, amount: f32) -> Self {
        Self::new(
            self.left() + amount / 2.0,
            self.top() + amount / 2.0,
            self.0.w - amount,
            self.0.h - amount,
        )
    }
}
