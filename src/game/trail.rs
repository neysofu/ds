//! Code for representing a path a dwarf can follow on a layer
use super::location::Location;
use std::collections::linked_list::Iter;
use std::collections::HashMap;
use std::collections::LinkedList;

#[derive(Debug, Default)]
pub struct Trail(LinkedList<Location>);

impl Trail {
    pub fn new() -> Self {
        Trail(LinkedList::new())
    }

    pub fn build(goal: Location, came_from: &HashMap<Location, Location>) -> Self {
        let mut trail = Self::new();
        trail.push_front(goal);

        let mut current = goal;
        while let Some(previous) = came_from.get(&current).cloned() {
            if current == previous {
                break;
            }
            trail.push_front(previous);
            current = previous;
        }

        // Trail build includes the start node.  We need to remove it
        // because we don't path to the start node.
        trail.pop_front();

        trail
    }

    pub fn push_front(&mut self, location: Location) {
        self.0.push_front(location);
    }

    pub fn push_back(&mut self, location: Location) {
        self.0.push_back(location);
    }

    pub fn pop_front(&mut self) -> Option<Location> {
        self.0.pop_front()
    }

    pub fn front(&self) -> Option<&Location> {
        self.0.front()
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.len() == 0
    }

    pub fn iter(&self) -> Iter<Location> {
        self.0.iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_delegation() {
        let mut trail = Trail::new();
        assert_eq!(trail.len(), 0);

        trail.push_back(Location::new(0, 0));
        trail.push_back(Location::new(0, 1));
        trail.push_back(Location::new(1, 1));
        assert_eq!(trail.len(), 3);

        let expected = vec![
            Location::new(0, 0),
            Location::new(0, 1),
            Location::new(1, 1),
        ];
        assert_eq!(trail.iter().copied().collect::<Vec<_>>(), expected);
    }

    #[test]
    fn test_build() {
        let mut came_from: HashMap<Location, Location> = HashMap::new();

        // When came_from is empty
        let mut trail = Trail::build(Location::new(10, 10), &came_from);
        assert_eq!(trail.iter().copied().collect::<Vec<Location>>(), vec![]);

        // With a trail of 1
        came_from.insert(Location::new(10, 10), Location::new(9, 9));
        trail = Trail::build(Location::new(10, 10), &came_from);
        assert_eq!(
            trail.iter().copied().collect::<Vec<Location>>(),
            vec![Location::new(10, 10)]
        );

        // With a trail of 2
        came_from.insert(Location::new(9, 9), Location::new(9, 8));
        trail = Trail::build(Location::new(10, 10), &came_from);
        assert_eq!(
            trail.iter().copied().collect::<Vec<Location>>(),
            vec![Location::new(9, 9), Location::new(10, 10)]
        );

        // With a disconnected node
        came_from.insert(Location::new(2, 2), Location::new(1, 1));
        trail = Trail::build(Location::new(10, 10), &came_from);
        assert_eq!(
            trail.iter().copied().collect::<Vec<Location>>(),
            vec![Location::new(9, 9), Location::new(10, 10)]
        );
    }

}
