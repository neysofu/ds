//! Code for returning `Symbols` from map locations on a particular map layer.
use super::location::Direction;
use super::location::Distance;
use super::location::Location;
use super::location::Offset;
use super::map::Map;
use super::map::MapTile;
use super::rng::Rng;
use super::symbol::Symbol;
use strum::IntoEnumIterator;
/// Represents a single layer of a map.

#[derive(Debug)]
pub struct Layer {
    pub dwarfs: Box<[bool]>,
    pub height: usize,
    pub map: Map,
    pub paths: Box<[bool]>,
    pub rngs: Box<[Rng]>,
    pub seed: String,
    pub width: usize,
}

fn create_rngs(width: usize, height: usize, mut rng: Rng) -> Box<[Rng]> {
    let mut rngs = Vec::with_capacity(width * height + 1);
    for _ in 0..=width * height {
        rng.jump();
        rngs.push(rng.clone());
    }
    rngs.into_boxed_slice()
}

impl Layer {
    /// Create a new layer with a specific height and width.
    pub fn new(width: usize, height: usize, seed: &str) -> Self {
        let rng = Rng::new(seed);
        let map = Map::new(width, height, &rng);
        let paths = map.paths();
        Layer {
            width,
            height,
            seed: seed.to_string(),
            rngs: create_rngs(width, height, rng.clone()),
            map,
            dwarfs: vec![false; width * height].into_boxed_slice(),
            paths,
        }
    }

    pub fn create_map_contents(&mut self) {
        self.map.create_contents();
    }

    /// Returns true if the map `Location` is contained in the layer.
    pub fn contains(&self, location: &Location) -> bool {
        location.x < self.width && location.y < self.height
    }

    /// Start at a location and move distance sqaures in a direction.
    /// It returns Some(location) if there is a tile on the layer at
    /// the destination.  Otherwise it returns None.  It only works
    /// in the directions of Up, Down, Left and Right.  Any other direction
    /// will return None.
    pub fn try_rook_move(
        &self,
        location: &Location,
        dir: Direction,
        distance: usize,
    ) -> Option<Location> {
        match dir {
            Direction::Left => location.checked_sub(Location::new(distance, 0)),
            Direction::Right => location.checked_add(Location::new(distance, 0)),
            Direction::Up => location.checked_sub(Location::new(0, distance)),
            Direction::Down => location.checked_add(Location::new(0, distance)),
            _ => None,
        }
        .filter(|l| self.contains(l))
    }

    /// Starts at a location.  Moves one square in the given direction.
    /// If there is no tile at the destination it returns None.
    pub fn try_walk(&self, location: &Location, dir: Direction) -> Option<Location> {
        match dir {
            Direction::Left => self.try_rook_move(&location, Direction::Left, 1),
            Direction::Right => self.try_rook_move(&location, Direction::Right, 1),
            Direction::Up => self.try_rook_move(&location, Direction::Up, 1),
            Direction::Down => self.try_rook_move(&location, Direction::Down, 1),
            Direction::UpLeft => {
                let l = self.try_walk(&location, Direction::Up)?;
                self.try_walk(&l, Direction::Left)
            }
            Direction::UpRight => {
                let l = self.try_walk(&location, Direction::Up)?;
                self.try_walk(&l, Direction::Right)
            }
            Direction::DownRight => {
                let l = self.try_walk(&location, Direction::Down)?;
                self.try_walk(&l, Direction::Right)
            }
            Direction::DownLeft => {
                let l = self.try_walk(&location, Direction::Down)?;
                self.try_walk(&l, Direction::Left)
            }
        }
        .filter(|l| self.contains(l))
    }

    pub fn try_warp(&self, location: &Location, offset: Offset) -> Option<Location> {
        let l = self.try_rook_move(&location, offset.horizontal.0, offset.horizontal.1)?;
        self.try_rook_move(&l, offset.vertical.0, offset.vertical.1)
    }

    pub fn exposed(&self, location: Location) -> bool {
        let layer_location = LayerLocation {
            layer: self,
            location,
        };
        layer_location.into_iter().any(|l| {
            if let Some(terrain) = self.map.get_tile(l) {
                terrain == MapTile::Floor
            } else {
                false
            }
        })
    }

    pub fn terrain_symbol(&self, location: Location) -> Option<Symbol> {
        let rng = self.rngs.get(location.index(self.width))?.clone();

        let terrain = self.map.get_tile(location)?;
        match terrain {
            MapTile::Terrain => {
                if self.exposed(location) {
                    Some(Symbol::wall())
                } else {
                    Some(Symbol::undiscovered_terrain(rng))
                }
            }
            MapTile::Floor => Some(Symbol::floor(rng)),
            MapTile::Exit => Some(Symbol::exit()),
        }
    }

    pub fn symbol(&self, location: Location) -> Option<Symbol> {
        if self.dwarf_here(location) {
            Some(Symbol::dwarf())
        } else {
            self.terrain_symbol(location)
        }
    }

    pub fn pathable(&self, location: Location) -> bool {
        if location.x >= self.width || location.y >= self.height {
            false
        } else {
            self.map.paths()[location.index(self.width)]
        }
    }

    pub fn path_cost(&self, from: Location, to: Location) -> Distance {
        if self.pathable(to) {
            from.distance_to(to)
        } else {
            std::f64::INFINITY
        }
    }

    pub fn place_exit(&mut self, location: Location) -> bool {
        if let Some(tile) = self.map.get_tile(location) {
            if tile == MapTile::Terrain && self.exposed(location) {
                self.map.set_tile(location, MapTile::Exit)
            } else {
                false
            }
        } else {
            false
        }
    }

    pub fn dwarf_here(&self, location: Location) -> bool {
        let index = location.index(self.width);
        self.dwarfs.get(index).copied().unwrap_or(false)
    }

    pub fn exit_here(&self, location: Location) -> bool {
        let index = location.index(self.width);
        self.map.get(index) == Some(MapTile::Exit)
    }

    pub fn clear_dwarfs(&mut self) {
        self.dwarfs = vec![false; self.width * self.height].into_boxed_slice();
    }

    pub fn set_dwarf(&mut self, location: Location) {
        self.dwarfs[location.index(self.width)] = true;
    }

    pub fn index(&self, location: Location) -> usize {
        location.index(self.width)
    }
}

#[derive(Clone, Copy)]
pub struct LayerLocation<'a> {
    pub layer: &'a Layer,
    pub location: Location,
}

pub struct SurroundingLocation<'a> {
    center: LayerLocation<'a>,
    direction_iter: <Direction as IntoEnumIterator>::Iterator,
}

impl<'a> Iterator for SurroundingLocation<'a> {
    type Item = Location;

    /// Returns the next `Grid` `Location` in breadth-wise order.
    /// If there are no more locations on the grid, returns `None`.
    fn next(&mut self) -> Option<Location> {
        let possible_direction = self.direction_iter.next();
        match possible_direction {
            None => None,
            Some(direction) => self.center.layer.try_walk(&self.center.location, direction),
        }
    }
}

impl<'a> IntoIterator for LayerLocation<'a> {
    type Item = Location;
    type IntoIter = SurroundingLocation<'a>;

    fn into_iter(self) -> Self::IntoIter {
        SurroundingLocation {
            center: self,
            direction_iter: Direction::iter(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_construct_layer() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let layer = Layer::new(width, height, seed);

        assert_eq!(layer.dwarfs.len(), 100);
    }

    #[test]
    fn test_create_rngs() {
        let seed = "test_seed";
        let height = 10;
        let width = 5;
        let seed_rng = Rng::new(seed);
        let rngs = create_rngs(width, height, seed_rng.clone());
        assert_eq!(rngs.len(), 51);
    }

    #[test]
    fn test_layer_contains() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let layer = Layer::new(width, height, seed);
        assert_eq!(layer.contains(&Location { x: 0, y: 0 }), true);
        assert_eq!(layer.contains(&Location { x: 9, y: 9 }), true);
        assert_eq!(layer.contains(&Location { x: 10, y: 9 }), false);
        assert_eq!(layer.contains(&Location { x: 9, y: 10 }), false);
        assert_eq!(layer.contains(&Location { x: 10, y: 10 }), false);
    }

    #[test]
    fn test_try_rook_move() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let layer = Layer::new(width, height, seed);
        let start = Location::new(5, 5);

        assert_eq!(
            layer.try_rook_move(&start, Direction::Left, 3),
            Some(Location::new(2, 5))
        );
        assert_eq!(
            layer.try_rook_move(&start, Direction::Up, 3),
            Some(Location::new(5, 2))
        );
        assert_eq!(
            layer.try_rook_move(&start, Direction::Right, 3),
            Some(Location::new(8, 5))
        );
        assert_eq!(
            layer.try_rook_move(&start, Direction::Down, 3),
            Some(Location::new(5, 8))
        );
    }

    #[test]
    fn test_try_walk_from_upper_left() {
        let seed = "test_seed";
        let height = 10;
        let width = 5;
        let layer = Layer::new(width, height, seed);
        let start = Location::new(0, 0);

        assert_eq!(layer.try_walk(&start, Direction::Left), None);
        assert_eq!(layer.try_walk(&start, Direction::UpLeft), None);
        assert_eq!(layer.try_walk(&start, Direction::Up), None);
        assert_eq!(layer.try_walk(&start, Direction::UpRight), None);
        assert_eq!(
            layer.try_walk(&start, Direction::Right),
            Some(Location::new(1, 0))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::DownRight),
            Some(Location::new(1, 1))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Down),
            Some(Location::new(0, 1))
        );
        assert_eq!(layer.try_walk(&start, Direction::DownLeft), None);
    }

    #[test]
    fn test_try_walk_from_lower_right() {
        let seed = "test_seed";
        let height = 10;
        let width = 5;
        let layer = Layer::new(width, height, seed);
        let start = Location::new(width - 1, height - 1);

        assert_eq!(
            layer.try_walk(&start, Direction::Left),
            Some(Location::new(3, 9))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::UpLeft),
            Some(Location::new(3, 8))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Up),
            Some(Location::new(4, 8))
        );
        assert_eq!(layer.try_walk(&start, Direction::UpRight), None);
        assert_eq!(layer.try_walk(&start, Direction::Right), None);
        assert_eq!(layer.try_walk(&start, Direction::DownRight), None);
        assert_eq!(layer.try_walk(&start, Direction::Down), None);
        assert_eq!(layer.try_walk(&start, Direction::DownLeft), None);
    }

    #[test]
    fn test_try_walk_from_middle() {
        let seed = "test_seed";
        let height = 10;
        let width = 5;
        let layer = Layer::new(width, height, seed);
        let start = Location::new(width / 2, height / 2);

        assert_eq!(
            layer.try_walk(&start, Direction::Left),
            Some(Location::new(1, 5))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::UpLeft),
            Some(Location::new(1, 4))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Up),
            Some(Location::new(2, 4))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::UpRight),
            Some(Location::new(3, 4))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Right),
            Some(Location::new(3, 5))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::DownRight),
            Some(Location::new(3, 6))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::Down),
            Some(Location::new(2, 6))
        );
        assert_eq!(
            layer.try_walk(&start, Direction::DownLeft),
            Some(Location::new(1, 6))
        );
    }

    #[test]
    fn test_try_warp() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let layer = Layer::new(width, height, seed);
        let start = Location::new(width / 2, height / 2);

        assert_eq!(
            layer.try_warp(
                &start,
                Offset {
                    vertical: (Direction::Left, 2),
                    horizontal: (Direction::Up, 2)
                }
            ),
            Some(Location::new(3, 3))
        );
        assert_eq!(
            layer.try_warp(
                &start,
                Offset {
                    vertical: (Direction::Right, 2),
                    horizontal: (Direction::Down, 2)
                }
            ),
            Some(Location::new(7, 7))
        );
        assert_eq!(
            layer.try_warp(
                &start,
                Offset {
                    vertical: (Direction::Right, 0),
                    horizontal: (Direction::Down, 0)
                }
            ),
            Some(Location::new(5, 5))
        );
        assert_eq!(
            layer.try_warp(
                &start,
                Offset {
                    vertical: (Direction::Left, 6),
                    horizontal: (Direction::Down, 0)
                }
            ),
            None
        );
        assert_eq!(
            layer.try_warp(
                &start,
                Offset {
                    vertical: (Direction::Left, 0),
                    horizontal: (Direction::Down, 6)
                }
            ),
            None
        );
        assert_eq!(
            layer.try_warp(
                &start,
                Offset {
                    vertical: (Direction::Right, 5),
                    horizontal: (Direction::Down, 0)
                }
            ),
            None
        );
        assert_eq!(
            layer.try_warp(
                &start,
                Offset {
                    vertical: (Direction::Right, 0),
                    horizontal: (Direction::Down, 5)
                }
            ),
            None
        );
    }

    #[test]
    fn test_pathable() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let layer = Layer::new(width, height, seed);

        // Locations outside of the layer are not pathable
        assert_eq!(layer.pathable(Location::new(15, 15)), false)
    }

    #[test]
    fn test_place_exit() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut layer = Layer::new(width, height, seed);

        layer.map.add_room(Location::new(5, 5), (1, 1));
        // On a wall
        assert_eq!(layer.place_exit(Location::new(4, 5)), true);
        // Inside a room
        assert_eq!(layer.place_exit(Location::new(5, 5)), false);
        // Unexposed dirt
        assert_eq!(layer.place_exit(Location::new(3, 3)), false);
    }

    #[test]
    fn test_terrain_symbol() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut layer = Layer::new(width, height, seed);

        layer.map.add_room(Location::new(5, 5), (1, 1));
        let rng = layer
            .rngs
            .get(Location::new(5, 5).index(layer.width))
            .unwrap()
            .clone();

        // Room is a floor
        assert_eq!(
            layer.terrain_symbol(Location::new(5, 5)),
            Some(Symbol::floor(rng))
        );

        // The terrain surrounding the floor is a wall
        assert_eq!(
            layer.terrain_symbol(Location::new(4, 4)),
            Some(Symbol::wall())
        );
        assert_eq!(
            layer.terrain_symbol(Location::new(4, 5)),
            Some(Symbol::wall())
        );
        assert_eq!(
            layer.terrain_symbol(Location::new(4, 6)),
            Some(Symbol::wall())
        );
        assert_eq!(
            layer.terrain_symbol(Location::new(5, 4)),
            Some(Symbol::wall())
        );
        assert_eq!(
            layer.terrain_symbol(Location::new(5, 6)),
            Some(Symbol::wall())
        );
        assert_eq!(
            layer.terrain_symbol(Location::new(6, 4)),
            Some(Symbol::wall())
        );
        assert_eq!(
            layer.terrain_symbol(Location::new(6, 5)),
            Some(Symbol::wall())
        );
        assert_eq!(
            layer.terrain_symbol(Location::new(6, 6)),
            Some(Symbol::wall())
        );

        assert_eq!(layer.place_exit(Location::new(4, 5)), true);
        assert_eq!(
            layer.terrain_symbol(Location::new(4, 5)),
            Some(Symbol::exit())
        );
    }
}
