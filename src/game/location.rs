//! Code for dealing with locations on a grid,
//! whether it be the `Grid` display or the map `Layer`
use self::Direction::*;
use std::ops::{Add, Neg};

pub type Distance = f64;

/// Direction from a location
#[derive(Clone, Copy, PartialEq, Eq, Debug, EnumIter)]
pub enum Direction {
    UpLeft,
    Up,
    UpRight,
    Right,
    DownRight,
    Down,
    DownLeft,
    Left,
}

impl Neg for Direction {
    type Output = Self;

    fn neg(self) -> Self {
        match self {
            Left => Right,
            Right => Left,
            Up => Down,
            Down => Up,
            UpLeft => DownRight,
            DownRight => UpLeft,
            UpRight => DownLeft,
            DownLeft => UpRight,
        }
    }
}

/// Offset on a `Grid` display or map `Layer`
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Offset {
    pub horizontal: (Direction, usize),
    pub vertical: (Direction, usize),
}

impl Neg for Offset {
    type Output = Offset;

    fn neg(self) -> Offset {
        Offset {
            horizontal: (-self.horizontal.0, self.horizontal.1),
            vertical: (-self.vertical.0, self.vertical.1),
        }
    }
}

/// Location on the `Grid` display or map `Layer`
#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash, PartialOrd, Ord)]
pub struct Location {
    pub x: usize,
    pub y: usize,
}

fn difference(a: usize, b: usize) -> usize {
    if a > b {
        a - b
    } else {
        b - a
    }
}

impl Location {
    /// Create a `Location` for the `Grid` display or map `Layer`
    pub fn new(x: usize, y: usize) -> Location {
        Location { x, y }
    }

    /// Try to subtract one location from another.
    /// Returns `None` if the result would be less than zero.
    pub fn checked_sub(self, other: Location) -> Option<Location> {
        Some(Self::new(
            self.x.checked_sub(other.x)?,
            self.y.checked_sub(other.y)?,
        ))
    }

    /// Try to add one location to another.
    /// Returns `None` if the result would overflow.
    pub fn checked_add(self, other: Location) -> Option<Location> {
        Some(Self::new(
            self.x.checked_add(other.x)?,
            self.y.checked_add(other.y)?,
        ))
    }

    /// Returns the index into a grid for a breadth-first traversal
    /// width is the width of the grid
    pub fn index(&self, width: usize) -> usize {
        self.y * width + self.x
    }

    /// Calculate the offset from another location
    pub fn offset_from(&self, other: Location) -> Offset {
        let horizontal = if self.x <= other.x {
            (Direction::Left, other.x - self.x)
        } else {
            (Direction::Right, self.x - other.x)
        };

        let vertical = if self.y <= other.y {
            (Direction::Up, other.y - self.y)
        } else {
            (Direction::Down, self.y - other.y)
        };

        Offset {
            horizontal,
            vertical,
        }
    }

    pub fn distance_to(&self, goal: Location) -> Distance {
        let x = difference(self.x, goal.x) as Distance;
        let y = difference(self.y, goal.y) as Distance;

        (x * x + y * y).sqrt()
    }
}

impl Add for Location {
    type Output = Location;

    /// Add two locations.
    fn add(self, other: Location) -> Location {
        Location::new(self.x + other.x, self.y + other.y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cmp::Ordering;

    #[test]
    fn test_index() {
        assert_eq!(Location::new(0, 0).index(10), 0);
        assert_eq!(Location::new(1, 0).index(10), 1);
        assert_eq!(Location::new(2, 0).index(10), 2);
        assert_eq!(Location::new(9, 0).index(10), 9);
        assert_eq!(Location::new(0, 1).index(10), 10);
        assert_eq!(Location::new(1, 1).index(10), 11);
    }

    #[test]
    fn index_middle() {
        let location = Location::new(0, 3);
        assert_eq!(location.index(10), 30);
    }

    #[test]
    fn test_distance_to() {
        assert_eq!(Location::new(1, 1).distance_to(Location::new(1, 1)), 0.0);
        assert_eq!(Location::new(1, 1).distance_to(Location::new(1, 2)), 1.0);
        assert_eq!(
            Location::new(1, 1).distance_to(Location::new(2, 2)),
            2.0_f64.sqrt()
        );
        assert_eq!(
            Location::new(1, 1).distance_to(Location::new(3, 4)),
            13.0_f64.sqrt()
        );

        assert_ne!(
            Location::new(0, 0).distance_to(Location::new(0, usize::max_value())),
            std::f64::INFINITY
        );
    }

    #[test]
    fn test_offset_neg() {
        let offset = Offset {
            horizontal: (Direction::Left, 5),
            vertical: (Direction::Down, 10),
        };
        assert_eq!(--offset, offset);

        assert_eq!(
            -Offset {
                horizontal: (Direction::Left, 3),
                vertical: (Direction::Up, 2),
            },
            Offset {
                horizontal: (Direction::Right, 3),
                vertical: (Direction::Down, 2),
            }
        );
        assert_eq!(
            -Offset {
                horizontal: (Direction::Right, 3),
                vertical: (Direction::Down, 2),
            },
            Offset {
                horizontal: (Direction::Left, 3),
                vertical: (Direction::Up, 2),
            }
        );
    }

    #[test]
    fn test_ord() {
        assert_eq!(
            Location::new(1, 1).cmp(&Location::new(2, 2)),
            Ordering::Less
        );
        assert_eq!(
            Location::new(2, 1).cmp(&Location::new(2, 2)),
            Ordering::Less
        );
        assert_eq!(
            Location::new(1, 2).cmp(&Location::new(2, 2)),
            Ordering::Less
        );
        assert_eq!(
            Location::new(2, 2).cmp(&Location::new(1, 1)),
            Ordering::Greater
        );
        assert_eq!(
            Location::new(2, 2).cmp(&Location::new(2, 1)),
            Ordering::Greater
        );
        assert_eq!(
            Location::new(2, 2).cmp(&Location::new(1, 2)),
            Ordering::Greater
        );
        assert_eq!(
            Location::new(1, 1).cmp(&Location::new(1, 1)),
            Ordering::Equal
        );
    }
}
