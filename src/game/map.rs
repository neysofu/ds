//! Represents a map on a layer

use super::location::Location;
use super::rand::Rng as RngTrait;
use super::rng::Rng;

/// The types of objects that might be in a map tile
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MapTile {
    Terrain,
    Floor,
    Exit,
}

/// Represents the terrain on a layer
#[derive(Debug)]
pub struct Map {
    pub width: usize,
    pub height: usize,
    pub rng: Rng,
    pub tiles: Box<[MapTile]>,
}

impl Map {
    pub fn new(width: usize, height: usize, rng: &Rng) -> Map {
        Map {
            width,
            height,
            rng: rng.clone(),
            tiles: vec![MapTile::Terrain; width * height].into_boxed_slice(),
        }
    }

    pub fn create_contents(&mut self) {
        let mut room_rng = self.rng.clone();
        let room_size = self.room_size(&mut room_rng);
        if let Some(room_location) = self.room_location(room_size, &mut room_rng) {
            self.add_room(room_location, room_size);
        }
    }

    fn room_size(&self, rng: &mut Rng) -> (usize, usize) {
        let room_width = rng.gen_range(2, 6);
        let room_height = rng.gen_range(2, 6);
        (room_width, room_height)
    }

    fn room_location(&self, size: (usize, usize), rng: &mut Rng) -> Option<Location> {
        if size.0 >= self.width || size.1 >= self.height {
            None
        } else {
            let max_x = self.width - size.0;
            let max_y = self.width - size.1;

            let x = rng.gen_range(1, max_x);
            let y = rng.gen_range(1, max_y);
            Some(Location::new(x, y))
        }
    }

    pub fn add_room(&mut self, location: Location, size: (usize, usize)) {
        let (room_width, room_height) = size;

        // Don't add a room if it doesn't fit on the map
        for i in location.x..location.x + room_width {
            for j in location.y..location.y + room_height {
                self.tiles[j * self.width + i] = MapTile::Floor;
            }
        }
    }

    pub fn set_tile(&mut self, location: Location, tile: MapTile) -> bool {
        self.index(location).map(|i| self.tiles[i] = tile).is_some()
    }

    pub fn get_tile(&self, location: Location) -> Option<MapTile> {
        let index = self.index(location)?;
        self.get(index)
    }

    /// Returns the map tile for an index into the map.
    /// The index is a breadth first index into the array.
    pub fn get(&self, index: usize) -> Option<MapTile> {
        self.tiles.get(index).copied()
    }

    pub fn index(&self, location: Location) -> Option<usize> {
        if location.x < self.width && location.y < self.height {
            Some(location.index(self.width))
        } else {
            None
        }
    }

    pub fn paths(&self) -> Box<[bool]> {
        let size = self.height * self.width;
        let mut paths = vec![false; size].into_boxed_slice();
        for i in 0..size {
            paths[i] = self.tiles[i] == MapTile::Floor;
        }
        paths
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_map_new() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let seed_rng = Rng::new(seed);
        let map = Map::new(width, height, &seed_rng);
        assert_eq!(map.tiles.len(), 100);
    }

    #[test]
    fn test_map_room_size() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let map = Map::new(width, height, &rng);
        let size = map.room_size(&mut map.rng.clone());

        assert_eq!(size, (5, 4));
    }

    #[test]
    fn test_map_room_location() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut rng = Rng::new(seed);
        let map = Map::new(width, height, &rng);
        let size = map.room_size(&mut rng);
        let room_location = map.room_location(size, &mut rng);

        assert_eq!(room_location, Some(Location::new(2, 1)));

        assert_eq!(size, (5, 4));
        assert_eq!(map.room_location((11, 5), &mut map.rng.clone()), None);
        assert_eq!(map.room_location((5, 11), &mut map.rng.clone()), None);
    }

    #[test]
    fn test_index() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let map = Map::new(width, height, &rng);

        assert_eq!(map.index(Location::new(0, 0)), Some(0));
        assert_eq!(map.index(Location::new(1, 1)), Some(11));
        assert_eq!(map.index(Location::new(10, 10)), None);
        assert_eq!(map.index(Location::new(10, 1)), None);
        assert_eq!(map.index(Location::new(1, 10)), None);
    }

    #[test]
    fn test_set_tile() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let mut map = Map::new(width, height, &rng);

        let index = map.index(Location::new(1, 1)).unwrap();
        assert_eq!(map.get(index), Some(MapTile::Terrain));
        assert_eq!(map.set_tile(Location::new(1, 1), MapTile::Floor), true);
        assert_eq!(map.get(index), Some(MapTile::Floor));

        assert_eq!(map.set_tile(Location::new(10, 10), MapTile::Floor), false);
    }

    #[test]
    fn test_add_room() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let mut map = Map::new(width, height, &rng);

        map.add_room(Location::new(0, 0), (2, 2));
        assert_eq!(
            map.get(Location::new(0, 0).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(0, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(0, 2).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(1, 0).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(1, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(1, 2).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(2, 0).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(2, 1).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(2, 2).index(width)),
            Some(MapTile::Terrain)
        );

        map.add_room(Location::new(2, 1), (2, 2));
        assert_eq!(
            map.get(Location::new(2, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(2, 2).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(2, 3).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(3, 1).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(3, 2).index(width)),
            Some(MapTile::Floor)
        );
        assert_eq!(
            map.get(Location::new(3, 3).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(4, 1).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(4, 2).index(width)),
            Some(MapTile::Terrain)
        );
        assert_eq!(
            map.get(Location::new(4, 3).index(width)),
            Some(MapTile::Terrain)
        );
    }

    #[test]
    fn test_paths() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let rng = Rng::new(seed);
        let mut map = Map::new(width, height, &rng);

        map.add_room(Location::new(0, 0), (2, 2));

        map.add_room(Location::new(0, 0), (2, 2));
        assert_eq!(map.paths()[Location::new(0, 0).index(width)], true);
        assert_eq!(map.paths()[Location::new(0, 1).index(width)], true);
        assert_eq!(map.paths()[Location::new(0, 2).index(width)], false);
        assert_eq!(map.paths()[Location::new(1, 0).index(width)], true);
        assert_eq!(map.paths()[Location::new(1, 1).index(width)], true);
        assert_eq!(map.paths()[Location::new(1, 2).index(width)], false);
        assert_eq!(map.paths()[Location::new(2, 0).index(width)], false);
        assert_eq!(map.paths()[Location::new(2, 1).index(width)], false);
        assert_eq!(map.paths()[Location::new(2, 2).index(width)], false);

        map.add_room(Location::new(2, 1), (2, 2));
        assert_eq!(map.paths()[Location::new(2, 1).index(width)], true);
        assert_eq!(map.paths()[Location::new(2, 2).index(width)], true);
        assert_eq!(map.paths()[Location::new(2, 3).index(width)], false);
        assert_eq!(map.paths()[Location::new(3, 1).index(width)], true);
        assert_eq!(map.paths()[Location::new(3, 2).index(width)], true);
        assert_eq!(map.paths()[Location::new(3, 3).index(width)], false);
        assert_eq!(map.paths()[Location::new(4, 1).index(width)], false);
        assert_eq!(map.paths()[Location::new(4, 2).index(width)], false);
        assert_eq!(map.paths()[Location::new(4, 3).index(width)], false);
    }
}
