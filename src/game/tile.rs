//! Code dealing with rending `Symbols` on the `Grid`
use ggez::graphics::spritebatch::SpriteBatch;
use ggez::graphics::{self, Color, DrawParam, FilterMode, Text};
use ggez::{Context, GameResult};

use super::point::Point;

/// Represents the `Text` and background color for a symbol to be
/// rendered on the screen at a given position (with offset).
pub struct Tile<'a> {
    text: &'a Option<Text>,
    background: Option<Color>,
    destination: Point,
    offset: Point,
}

impl<'a> Tile<'a> {
    pub fn new(
        text: &'a Option<Text>,
        background: Option<Color>,
        em: f32,
        padding: f32,
        destination: Point,
    ) -> Tile<'a> {
        let offset = Point::new(0.0, em * (padding - 1.0) / 2.0);

        Tile {
            text,
            background,
            destination,
            offset,
        }
    }

    pub fn draw_queued(ctx: &mut Context, background_batch: &SpriteBatch) -> GameResult<()> {
        graphics::draw(ctx, background_batch, graphics::DrawParam::default())?;
        graphics::draw_queued_text(ctx, DrawParam::default(), None, FilterMode::Linear)
    }

    pub fn queue(&self, ctx: &mut Context, background_batch: &mut SpriteBatch) -> GameResult<()> {
        if let Some(ref background) = self.background {
            background_batch.add(
                graphics::DrawParam::default()
                    .color(*background)
                    .dest(self.destination),
            );
        }
        if let Some(ref text) = self.text {
            graphics::queue_text(ctx, text, self.destination + self.offset, None);
        }
        Ok(())
    }
}
