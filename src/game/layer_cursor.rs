//! Code for translating grid locations to layer locations.
use super::grid::Grid;
use super::layer::Layer;
use super::location::Location;
use super::symbol::Symbol;

#[derive(Clone, Copy, Debug)]
/// Allows you to convert from Grid coordinates to Layer coordinates.
/// When the layer_offset is (0, 0), it draws the upper left hand
/// corner of the layer in the middle of the grid.  The offset
/// moves the layer up and to the left.
pub struct LayerCursor<'a, 'b> {
    pub layer_offset: Location,
    pub grid: &'a Grid,
    pub layer: &'b Layer,
}

impl<'a, 'b> LayerCursor<'a, 'b> {
    pub fn new(layer_offset: Location, grid: &'a Grid, layer: &'b Layer) -> LayerCursor<'a, 'b> {
        LayerCursor {
            layer_offset,
            grid,
            layer,
        }
    }

    /// Calculates the layer location given a grid location.
    /// If the layer doesn't extend to this part of the grid, returns None.
    pub fn layer_location(&self, grid_location: Location) -> Option<Location> {
        grid_location
            .checked_add(self.layer_offset)?
            .checked_sub(self.grid.center())
            .filter(|l| self.layer.contains(l))
    }

    /// Returns the symbol on the map at the given grid location.
    /// If the grid location is in the center of the map, return the cursor.
    /// If the layer doesn't extend to this part of the grid, return the off_map symbol.
    /// Returns None if there is an error somewhere.
    pub fn symbol(&self, grid_location: Location) -> Option<Symbol> {
        if grid_location == self.grid.center() {
            Some(Symbol::cursor())
        } else {
            match self.layer_location(grid_location) {
                None => Some(Symbol::off_map()),
                Some(layer_location) => self.layer.symbol(layer_location),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use game::font::Font;
    use game::rect::Rect;

    #[test]
    fn test_orig_layer_location() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let font_size = 26.0;
        let padding = 1.2;
        let font = Font::new(None, None, font_size);
        let grid = Grid::new(
            Rect::new(
                0.0,
                0.0,
                height as f32 * font_size * padding,
                width as f32 * font_size * padding,
            ),
            font,
            padding,
        );
        let layer = Layer::new(width * 2, height * 2, seed);
        let cursor = LayerCursor::new(Location::new(0, 0), &grid, &layer);
        assert_eq!(cursor.layer_location(Location::new(0, 0)), None);
    }

    #[test]
    fn test_moved_layer_location() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let font_size = 26.0;
        let padding = 1.2;
        let font = Font::new(None, None, font_size);
        let grid = Grid::new(
            Rect::new(
                0.0,
                0.0,
                height as f32 * font_size * padding,
                width as f32 * font_size * padding,
            ),
            font,
            padding,
        );
        let layer = Layer::new(width * 2, height * 2, seed);
        let cursor = LayerCursor::new(Location::new(5, 5), &grid, &layer);
        // Shows the location on the layer for grid location (0, 0)
        // When the layer has been moved up and to the left by 5 tiles.
        // This should show the upper left location on the layer.
        assert_eq!(
            cursor.layer_location(Location::new(0, 0)),
            Some(Location::new(0, 0))
        );
    }

    #[test]
    fn test_indexed_layer_location() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let font_size = 26.0;
        let padding = 1.2;
        let font = Font::new(None, None, font_size);
        let grid = Grid::new(
            Rect::new(
                0.0,
                0.0,
                height as f32 * font_size * padding,
                width as f32 * font_size * padding,
            ),
            font,
            padding,
        );
        let layer = Layer::new(width * 2, height * 2, seed);
        let cursor = LayerCursor::new(Location::new(0, 0), &grid, &layer);
        assert_eq!(
            cursor.layer_location(Location::new(5, 5)),
            Some(Location::new(0, 0))
        );
    }
}
