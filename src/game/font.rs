//! Wrapper for fonts.
use ggez::graphics;

#[derive(Clone, Copy, Debug)]
pub struct Font {
    pub regular: Option<graphics::Font>,
    pub bold: Option<graphics::Font>,
    pub size: f32,
}

/// Holds the font for rendering text.
impl Font {
    pub fn new(regular: Option<graphics::Font>, bold: Option<graphics::Font>, size: f32) -> Self {
        Font {
            regular,
            bold,
            size,
        }
    }
}
