use super::layer::Layer;
use super::layer::LayerLocation;
use super::location::Distance;
use super::location::Location;
use super::trail::Trail;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct Node {
    pub location: Location,
    pub score: u64,
}

impl Node {
    pub fn new(location: Location, score: u64) -> Self {
        Node { location, score }
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other
            .score
            .cmp(&self.score)
            .then_with(|| self.location.cmp(&other.location))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Default)]
struct CandidateQueue(BinaryHeap<Node>);

impl CandidateQueue {
    fn score(distance_to_start: Distance, distance_to_goal: Distance) -> u64 {
        let distance = distance_to_start + distance_to_goal;

        if distance >= usize::max_value() as Distance {
            u64::max_value()
        } else {
            distance.trunc() as u64
        }
    }

    pub fn push(
        &mut self,
        location: Location,
        distance_to_start: Distance,
        distance_to_goal: Distance,
    ) {
        self.0.push(Node::new(
            location,
            Self::score(distance_to_start, distance_to_goal),
        ));
    }

    pub fn pop(&mut self) -> Option<Location> {
        self.0.pop().map(|node| node.location)
    }
}

#[derive(Default)]
struct DistanceMap(HashMap<Location, Distance>);

impl DistanceMap {
    pub fn insert(&mut self, location: Location, distance: Distance) {
        self.0.insert(location, distance);
    }

    pub fn get(&self, location: Location) -> Distance {
        self.0.get(&location).copied().unwrap_or(std::f64::INFINITY)
    }
}

struct State {
    goal: Location,
    open: CandidateQueue,
    start_distances: DistanceMap,
    closed: HashSet<Location>,
    came_from: HashMap<Location, Location>,
}

impl State {
    pub fn new(goal: Location) -> Self {
        State {
            goal,
            open: CandidateQueue::default(),
            start_distances: DistanceMap::default(),
            closed: HashSet::new(),
            came_from: HashMap::new(),
        }
    }

    pub fn add_candidate(&mut self, location: Location, distance_to_start: Distance) {
        self.open
            .push(location, distance_to_start, location.distance_to(self.goal));
        self.start_distances.insert(location, distance_to_start);
    }

    pub fn next_candidate(&mut self) -> Option<Location> {
        self.open.pop()
    }

    // We have finished checking this location for a viable path
    pub fn checked(&mut self, location: Location) {
        self.closed.insert(location);
    }

    pub fn is_checked(&self, location: Location) -> bool {
        self.closed.contains(&location)
    }

    pub fn link(&mut self, from: Location, to: Location) {
        self.came_from.insert(from, to);
    }

    pub fn closer_distance(
        &self,
        current: Location,
        neighbour: Location,
        layer: &Layer,
    ) -> Option<Distance> {
        let current_distance = self.start_distances.get(current);
        let start_distance = current_distance + layer.path_cost(current, neighbour);
        let location_distance = self.start_distances.get(neighbour);

        if start_distance < location_distance {
            Some(start_distance)
        } else {
            None
        }
    }
}

pub fn create_trail(start: Location, goal: Location, layer: &Layer) -> Trail {
    let mut state = State::new(goal);
    state.add_candidate(start, 0.0);

    while let Some(current) = state.next_candidate() {
        if current == goal {
            break;
        }

        state.checked(current);

        let layer_location = LayerLocation {
            layer,
            location: current,
        };
        // Check the locations surrounding the location
        for neighbour in layer_location {
            if !state.is_checked(neighbour) {
                if let Some(distance) = state.closer_distance(current, neighbour, layer) {
                    // FIXME: Remove the node from the open set if it exists
                    state.add_candidate(neighbour, distance);
                    state.link(neighbour, current);
                }
            }
        }
    }

    Trail::build(goal, &state.came_from)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_candidate_queue_score() {
        let location = Location::new(0, 0);
        let goal = Location::new(0, 0);
        assert_eq!(CandidateQueue::score(0.0, location.distance_to(goal)), 0);

        let location = Location::new(5, 0);
        let goal = Location::new(10, 0);
        assert_eq!(CandidateQueue::score(5.0, location.distance_to(goal)), 10);

        let location = Location::new(0, 0);
        let goal = Location::new(usize::max_value(), 0);

        assert_eq!(
            CandidateQueue::score(5.0, location.distance_to(goal)),
            u64::max_value()
        );
    }

    #[test]
    fn test_a_star() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut layer = Layer::new(width, height, seed);
        layer.map.add_room(Location::new(1, 1), (8, 8));
        assert_eq!(
            create_trail(Location::new(1, 1), Location::new(1, 1), &layer).len(),
            0
        );

        assert_eq!(
            create_trail(Location::new(1, 1), Location::new(1, 2), &layer).len(),
            1
        );

        assert_eq!(
            create_trail(Location::new(1, 1), Location::new(2, 3), &layer).len(),
            2
        );

        assert_eq!(
            create_trail(Location::new(1, 1), Location::new(8, 8), &layer).len(),
            7
        );

        // This is in the wall so it should not be pathable
        assert_eq!(
            create_trail(Location::new(1, 1), Location::new(9, 9), &layer).len(),
            0
        );
    }
}
