//! Eventually will hold code for dealing with the UI Frame.
//! At the moment it is a bit bare.
use game::font::Font;
use game::rect::Rect;
use ggez::graphics::{
    self, Align, Color, DrawMode, DrawParam, Mesh, MeshBuilder, Scale, Text, TextFragment,
};
use ggez::{Context, GameResult};

use super::point::Point;

/// Border for the UI.  Contains the FPS display.
pub struct Border {
    rect: Rect,
    font: Font,
    fps: f64,
}

pub struct GridPane<'a> {
    pub parent: &'a Border,
    pub rect: Rect,
}

impl<'a> GridPane<'a> {
    pub fn new(parent: &'a Border) -> Self {
        let start = parent.outer_line_width() + 1.0;
        let rect = Rect::new(
            start,
            start,
            (parent.rect.width() / 1.5).round(),
            (parent.rect.height() / 1.25).round(),
        );
        Self { parent, rect }
    }

    pub fn rhs_divider(&self) -> Rect {
        Rect::new(
            self.rect.right() + 1.0,
            self.parent.rect.top(),
            self.parent.outer_line_width() / 2.0,
            self.parent.rect.height(),
        )
    }
}

pub struct MenuPane<'a> {
    pub parent: &'a Border,
    pub rect: Rect,
}

impl<'a> MenuPane<'a> {
    pub fn new(parent: &'a Border) -> Self {
        let divider = parent.grid_pane().rhs_divider();
        let rect = Rect::new(
            divider.left() + 1.0,
            parent.outer_line_width() + 1.0,
            parent.rect.right() - divider.left() - (parent.outer_line_width() / 2.0),
            parent.rect.height() - parent.outer_line_width(),
        );
        Self { parent, rect }
    }
}

impl Border {
    const PADDING: f32 = 1.2;

    /// Initialise new FPS text somewhere on the screen.
    pub fn new(font: Font, destination: Point, width: f32, height: f32) -> Border {
        Border {
            rect: Rect::new(destination.x(), destination.y(), width, height)
                .shrink_by(font.size * Self::PADDING),
            fps: 0.0,
            font,
        }
    }

    pub fn update_fps(&mut self, fps: f64) {
        self.fps = fps;
    }

    pub fn fps_text(&self) -> Text {
        Text::new(graphics::TextFragment {
            text: format!("FPS: {}", self.fps.round() as u32),
            color: Some(Color::new(1.0, 1.0, 1.0, 1.0)),
            font: self.font.regular,
            scale: Some(Scale::uniform(self.font.size)),
        })
    }

    pub fn title_text(&self) -> Text {
        let mut text = Text::new(TextFragment {
            text: "Chibi Shiro".to_string(),
            color: Some(Color::new(1.0, 1.0, 1.0, 1.0)),
            font: self.font.bold,
            scale: Some(Scale::uniform(self.font.size)),
        });
        text.set_bounds(
            Point::new(self.rect.right(), self.outer_line_width()),
            Align::Center,
        );
        text
    }

    fn build_mesh(&self, rect: Rect, ctx: &mut Context) -> GameResult<Mesh> {
        let mut mb = MeshBuilder::new();

        let color = Color::new(0.1, 0.4, 0.3, 1.0);
        mb.rectangle(
            DrawMode::stroke(self.outer_line_width()),
            rect.into(),
            color,
        );

        mb.build(ctx)
    }

    fn build_divider_mesh(&self, ctx: &mut Context) -> GameResult<Mesh> {
        let mut mb = MeshBuilder::new();
        let rect = self.menu_divider();
        let ux = rect.left() - rect.width() / 2.0;
        let uy = rect.top();
        let lx = rect.left() - rect.width() / 2.0;
        let ly = rect.bottom();
        let line = [Point::new(ux, uy), Point::new(lx, ly)];

        let color = Color::new(0.1, 0.4, 0.3, 1.0);
        mb.line(&line, self.outer_line_width() / 2.0, color)?;

        mb.build(ctx)
    }

    /// Point where we draw the border in screen coordinates
    pub fn destination(&self) -> Point {
        Point::new(self.rect.left(), self.rect.top())
    }

    /// Point where we draw the text in the border in screen coordinates
    fn title_text_destination(&self) -> Point {
        let width = (self.outer_line_width() / 2.0).round();
        let offset = (self.font.size * ((Self::PADDING - 1.0) / 2.0)).round();
        self.destination() - Point::new(width, width - offset)
    }

    pub fn grid_pane(&self) -> GridPane {
        GridPane::new(self)
    }

    pub fn menu_pane(&self) -> MenuPane {
        MenuPane::new(self)
    }

    pub fn grid_size(&self) -> Rect {
        self.grid_pane().rect
    }

    pub fn menu_divider(&self) -> Rect {
        self.grid_pane().rhs_divider()
    }

    pub fn menu_size(&self) -> Rect {
        self.menu_pane().rect
    }

    pub fn outer_line_width(&self) -> f32 {
        self.font.size * Self::PADDING
    }

    pub fn inner_line_width(&self) -> f32 {
        self.font.size / 2.0
    }

    /// Draw the frame on the screen.
    pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        let params = DrawParam::default().dest(self.title_text_destination());
        let grid = self.build_mesh(self.rect, ctx)?;
        let divider = self.build_divider_mesh(ctx)?;
        graphics::draw(ctx, &grid, DrawParam::default())?;
        graphics::draw(ctx, &self.fps_text(), params)?;
        graphics::draw(ctx, &self.title_text(), params)?;
        graphics::draw(ctx, &divider, DrawParam::default())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn setup() -> Border {
        let font_size = 10.0;
        let font = Font::new(None, None, font_size);
        let destination = Point::new(0.0, 0.0);
        let width = 100.0;
        let height = 100.0;
        Border::new(font, destination, width, height)
    }

    #[test]
    fn test_update_fps() {
        let mut border = setup();
        // At first default to 0.0 fps
        assert_eq!(border.fps, 0.0);

        border.update_fps(60.9);
        assert_eq!(border.fps, 60.9);
    }

    #[test]
    fn test_fps_text() {
        let mut border = setup();
        // At first default to 0.0 fps
        assert_eq!(border.fps_text().contents(), "FPS: 0");

        // It uses the floor of the value
        border.update_fps(60.9);
        assert_eq!(border.fps_text().contents(), "FPS: 61");
    }

    #[test]
    fn test_destination() {
        let border = setup();
        assert_eq!(border.destination(), Point::new(6.0, 6.0));
    }

    #[test]
    fn test_title_text_destination() {
        let border = setup();
        assert_eq!(border.title_text_destination(), Point::new(0.0, 1.0));
    }

    #[test]
    fn test_menu_divider() {
        let border = setup();
        assert_eq!(border.menu_divider(), Rect::new(73.0, 6.0, 6.0, 88.0));
    }

    #[test]
    fn test_menu_size() {
        let border = setup();
        assert_eq!(border.menu_size(), Rect::new(80.0, 13.0, 13.0, 74.0));
    }

    #[test]
    fn test_grid_pane_constuction() {
        let border = setup();
        let grid_pane = border.grid_pane();
        assert_eq!(grid_pane.rect, Rect::new(13.0, 13.0, 59.0, 70.0));
    }
}
