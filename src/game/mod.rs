//! # DS Game Module
//!
//! This is the start of a reuse library for
//! making the game.

extern crate ggez;
extern crate rand;

pub mod a_star;
pub mod border;
pub mod dwarf;
pub mod font;
pub mod grid;
pub mod layer;
pub mod layer_cursor;
pub mod location;
pub mod map;
pub mod point;
pub mod rect;
pub mod rng;
pub mod symbol;
pub mod tile;
pub mod trail;

use ggez::conf::{ModuleConf, WindowMode, WindowSetup};
use ggez::event::EventsLoop;
use ggez::ContextBuilder;
use ggez::{error::GameError, Context, GameResult};
use std::env;
use std::path;

/// Initialise the game.
pub fn init(game_id: &'static str) -> GameResult<(Context, EventsLoop)> {
    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        ContextBuilder::new(game_id, "mikekchar")
            .window_setup(WindowSetup::default().title("Chibi Shiro"))
            .window_mode(
                WindowMode::default()
                    .dimensions(800.0, 600.0)
                    .resizable(true),
            )
            .modules(ModuleConf {
                gamepad: false,
                audio: false,
            })
            .add_resource_path(&path)
            .build()
    } else {
        Err(GameError::ConfigError(String::from(
            "Could not open manifest directory.",
        )))
    }
}
