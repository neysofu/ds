//! Code for representing a dwarf
use super::a_star;
use super::layer::Layer;
use super::location::Location;
use super::trail::Trail;

#[derive(Debug)]
pub struct Dwarf {
    pub location: Location,
    pub trail: Option<Trail>,
    // Percentage of a tile the dwarf can move per frame
    pub speed: f32,
    // Percentage in a tile the dwarf has moved so far towards its goal
    pub progress: f32,
}

impl Dwarf {
    pub fn new(location: Location) -> Self {
        Dwarf {
            location,
            trail: None,
            speed: 0.1,
            progress: 0.0,
        }
    }

    pub fn add_test_trail(&mut self, layer: &Layer) {
        let goal = self.location.checked_sub(Location::new(1, 2)).unwrap();
        self.trail = Some(a_star::create_trail(self.location, goal, layer));
    }

    // Pops next location off of the trail if it exists
    fn next_step(&mut self, layer: &Layer) -> Option<Location> {
        self.trail
            .as_mut()
            .and_then(|trail| trail.pop_front())
            .and_then(|location| {
                if layer.pathable(location) {
                    Some(location)
                } else {
                    self.trail = None;
                    None
                }
            })
    }

    fn update_progress(&mut self) -> usize {
        self.progress += self.speed;
        let steps = self.progress.trunc() as usize;
        self.progress = self.progress.fract();
        steps
    }

    pub fn walk(&mut self, layer: &Layer) -> bool {
        let mut updated = false;

        let steps = self.update_progress();
        for _ in 0..steps {
            if let Some(location) = self.next_step(layer) {
                self.location = location;
                updated = true;
            } else {
                updated = false;
            }
        }

        updated
    }
}

#[derive(Debug, Default)]
pub struct Population(Vec<Dwarf>);

impl Population {
    pub fn push(&mut self, value: Dwarf) {
        self.0.push(value);
    }

    pub fn iter_mut(&mut self) -> std::slice::IterMut<'_, Dwarf> {
        self.into_iter()
    }

    pub fn iter(&self) -> std::slice::Iter<'_, Dwarf> {
        self.into_iter()
    }

    pub fn get(&mut self, index: usize) -> Option<&Dwarf> {
        self.0.get(index)
    }

    pub fn get_mut(&mut self, index: usize) -> Option<&mut Dwarf> {
        self.0.get_mut(index)
    }

    pub fn add_dwarf(&mut self, location: Location, layer: &mut Layer) -> bool {
        let index = location.index(layer.width);

        if index >= layer.dwarfs.len() || !layer.pathable(location) {
            false
        } else {
            let mut dwarf = Dwarf::new(location);
            dwarf.add_test_trail(layer);
            self.push(dwarf);
            layer.set_dwarf(location);
            true
        }
    }

    pub fn update_layer(&self, layer: &mut Layer) {
        layer.clear_dwarfs();

        for dwarf in self.iter() {
            layer.set_dwarf(dwarf.location);
        }
    }

    pub fn tick(&mut self, layer: &Layer) -> bool {
        self.iter_mut()
            .fold(false, |d, dwarf| dwarf.walk(layer) || d)
    }
}

impl<'a> IntoIterator for &'a Population {
    type Item = &'a Dwarf;
    type IntoIter = std::slice::Iter<'a, Dwarf>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl<'a> IntoIterator for &'a mut Population {
    type Item = &'a mut Dwarf;
    type IntoIter = std::slice::IterMut<'a, Dwarf>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter_mut()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use game::symbol::Symbol;

    #[test]
    fn test_population_iter() {
        let mut population = Population::default();
        population.push(Dwarf::new(Location::new(0, 0)));
        population.push(Dwarf::new(Location::new(1, 1)));

        let locations: Vec<Location> = population.into_iter().map(|d| d.location).collect();
        assert_eq!(locations.get(0).copied(), Some(Location::new(0, 0)));
        assert_eq!(locations.get(1).copied(), Some(Location::new(1, 1)));
    }

    #[test]
    fn test_add_dwarf() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut population = Population::default();
        let mut layer = Layer::new(width, height, seed);

        layer.map.add_room(Location::new(5, 5), (1, 1));

        assert_eq!(
            population.add_dwarf(Location::new(100, 1), &mut layer),
            false
        );
        assert_eq!(layer.dwarf_here(Location::new(100, 1)), false);
        assert_eq!(
            population.add_dwarf(Location::new(1, 100), &mut layer),
            false
        );
        assert_eq!(layer.dwarf_here(Location::new(1, 100)), false);

        // There is a room at 5, 5
        assert_eq!(population.add_dwarf(Location::new(5, 5), &mut layer), true);
        assert_eq!(layer.dwarf_here(Location::new(5, 5)), true);
        assert_eq!(layer.symbol(Location::new(5, 5)), Some(Symbol::dwarf()));

        // But there is no room at 4, 4
        assert_eq!(population.add_dwarf(Location::new(4, 4), &mut layer), false);
        assert_eq!(layer.dwarf_here(Location::new(4, 4)), false);
        assert_ne!(layer.symbol(Location::new(4, 4)), None);
    }

    #[test]
    fn test_update_layer() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut population = Population::default();
        let mut layer = Layer::new(width, height, seed);
        layer.map.add_room(Location::new(1, 1), (8, 8));

        assert_eq!(population.add_dwarf(Location::new(2, 2), &mut layer), true);
        assert_eq!(population.add_dwarf(Location::new(3, 3), &mut layer), true);
        assert_eq!(population.add_dwarf(Location::new(4, 4), &mut layer), true);

        population.get_mut(0).unwrap().location = Location::new(6, 6);
        population.get_mut(1).unwrap().location = Location::new(7, 7);
        population.get_mut(2).unwrap().location = Location::new(8, 8);

        population.update_layer(&mut layer);

        assert_eq!(layer.dwarf_here(Location::new(2, 2)), false);
        assert_eq!(layer.dwarf_here(Location::new(6, 6)), true);
        assert_eq!(layer.dwarf_here(Location::new(3, 3)), false);
        assert_eq!(layer.dwarf_here(Location::new(7, 7)), true);
        assert_eq!(layer.dwarf_here(Location::new(4, 4)), false);
        assert_eq!(layer.dwarf_here(Location::new(8, 8)), true);
    }

    #[test]
    fn test_tick() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut population = Population::default();
        let mut layer = Layer::new(width, height, seed);
        layer.map.add_room(Location::new(1, 1), (8, 8));

        assert_eq!(population.add_dwarf(Location::new(2, 2), &mut layer), true);
        assert_eq!(layer.dwarf_here(Location::new(2, 2)), true);
        population.get_mut(0).unwrap().speed = 1.0;

        // Stepping when the dwarf doesn't move should not be dirty
        population.get_mut(0).unwrap().trail = None;
        assert_eq!(population.tick(&layer), false);

        population.get_mut(0).unwrap().trail = Some(Trail::new());
        assert_eq!(population.tick(&layer), false);

        // Stepping with a non-empty trail should be dirty
        population.get_mut(0).unwrap().trail = Some(Trail::new());
        population
            .get_mut(0)
            .unwrap()
            .trail
            .as_mut()
            .unwrap()
            .push_back(Location::new(2, 3));
        assert_eq!(population.tick(&layer), true);
        population.update_layer(&mut layer);
        // Dwarf should no longer be in 2, 2
        assert_eq!(layer.dwarf_here(Location::new(2, 2)), false);

        // Step eats a step, so put another location in the trail
        population
            .get_mut(0)
            .unwrap()
            .trail
            .as_mut()
            .unwrap()
            .push_back(Location::new(3, 3));
        // Add another dwarf without a trail
        assert_eq!(population.add_dwarf(Location::new(2, 2), &mut layer), true);

        // Because one dwarf moves, step should return dirty
        assert_eq!(population.tick(&layer), true);
    }

    #[test]
    fn test_update_progress() {
        let mut dwarf = Dwarf::new(Location::new(2, 2));

        // Initial speed and progress for a dwarf
        assert_eq!(dwarf.speed, 0.1);
        assert_eq!(dwarf.progress, 0.0);

        assert_eq!(dwarf.update_progress(), 0);
        assert_eq!(dwarf.progress, 0.1);

        for _ in 1..=8 {
            assert_eq!(dwarf.update_progress(), 0);
        }
        assert_eq!((dwarf.progress * 10.0).trunc() / 10.0, 0.9);
        assert_eq!(dwarf.update_progress(), 1);
        assert_eq!((dwarf.progress * 10.0).trunc() / 10.0, 0.0);
    }

    #[test]
    fn test_walk() {
        let seed = "test_seed";
        let height = 10;
        let width = 10;
        let mut layer = Layer::new(width, height, seed);
        layer.map.add_room(Location::new(1, 1), (8, 8));

        let mut dwarf = Dwarf::new(Location::new(2, 2));
        dwarf.speed = 1.0;

        // Without a trail
        assert!(dwarf.trail.is_none());
        assert_eq!(dwarf.walk(&layer), false);
        assert!(dwarf.trail.is_none());

        // With an empty trail
        dwarf.trail = Some(Trail::new());
        assert!(dwarf.trail.as_ref().unwrap().is_empty());
        assert_eq!(dwarf.walk(&layer), false);
        assert!(dwarf.trail.as_ref().unwrap().is_empty());

        // With an full trail
        let mut trail = Trail::new();
        trail.push_back(Location::new(2, 3));
        trail.push_back(Location::new(3, 3));
        dwarf.trail = Some(trail);

        assert_eq!(dwarf.walk(&layer), true);
        assert_eq!(dwarf.location, Location::new(2, 3));
        assert_eq!(dwarf.walk(&layer), true);
        assert_eq!(dwarf.location, Location::new(3, 3));
        assert_eq!(dwarf.walk(&layer), false);
        assert!(dwarf.trail.as_ref().unwrap().is_empty());

        // Try to walk into a wall
        dwarf.location = Location::new(1, 1);
        let mut trail = Trail::new();
        trail.push_back(Location::new(1, 0));
        trail.push_back(Location::new(0, 0));
        dwarf.trail = Some(trail);
        assert_eq!(dwarf.walk(&layer), false);
        assert_eq!(dwarf.location, Location::new(1, 1));
        assert_eq!(dwarf.trail.is_some(), false);
    }
}
