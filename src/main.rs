//! # Dwarf Startup
//!
//! A parady of Dwarf Fortress.  A group of dwarfs try to build a software
//! based company from scratch.  Will you be able to tame a unicorn?
//!
//! This is, in fact, simply a stop on the way to my actual goal: a full
//! fledged game that captures the spirit of Dwarf Fortress, but
//! with a different set of development priorities.  I'm still at a very
//! early stage of development, so expect everything above to change over
//! time.

extern crate ggez;
extern crate strum;
#[macro_use]
extern crate strum_macros;

use ggez::event;

pub mod chibi_shiro;
pub mod game;
use chibi_shiro::MainState;
use game::init;

pub fn main() {
    let (ctx, events) = &mut init("chibi-shiro").unwrap();
    let state = &mut MainState::new(ctx).unwrap();

    if let Err(e) = event::run(ctx, events, state) {
        println!("Error encountered: {}", e);
    } else {
        println!("Game exited cleanly.");
    }
}
