# Chibi Shiro

Originally called "Dwarf Startup".  A Dwarf Fortress like game written
in Rust.  I think the best way to think of this is to think, "What if
I wrote Dwarf Fortress from scratch, but made different choices along
the way".  The idea is *not* to make the same game, but I'm using it
as a starting point to help me explore my own ideas. I'm still at a
very early stage of development, so expect everything to change
over time.

## Streaming News

(Messages for the stream will appear here from time to time)

2019-12-05 07:57 - Streaming now.

### Next stream

  - Tokyo:         Friday      08:00
  - London:        Thursday    23:00
  - New York:      Thursday    18:00
  - San Francisco: Thursday    15:00

### Where to see the stream

I'm currently streaming the development of this game on Twitch
every weekday from 8 am - 10 am Japan time (GMT+9).  Feel free
to drop by: https://www.twitch.tv/urouroniwa

Twitch keeps previous videos for 2 weeks: https://www.twitch.tv/urouroniwa/videos

Keep in mind that I'm a relative novice with respect to Rust
development and game development in general, so at the moment the pace
is slow ;-) Hints and suggestions are always welcome, but I reserve
the right to make weird choices for no apparent reason (or because
it's !FUN!).

## Current Status

Running the application you can see a single layer that will eventually
be mineable.  The current position is marked by a yellow `X`.  Valid
mineable areas are black and the "edge of the map" is grey.  The display
grid is drawn in blue.  Each box on the grid can potentially contain a
character (though it doesn't yet).

If you use the vi-keys (hjkl) you can move the cursor around the mineable
parts of the layer.  The cursor actually stays in the centre of the
grid while the map moves.  You can also click on a tile with the mouse
and the cursor will move to that tile (although it is a little jarring
due to the lack of any animation.

The current FPS is displayed in the upper left corner.  It tries to maintain
around 60 FPS if it can.  You can resize the window and the grid will resize
to accommodate.

There is a room in the centre of the map.  You can place a dwarf in the
room by positioning the cursor in the room and pressing the 'd' button.
If it has room, it will run up to the north west a few squares, otherwise
it will just stand there.

You can zoom the grid using the scroll wheel on your mouse.

Pressing 'Q' or 'Esc' will end the game.

### Known issues

- Keypresses are not auto-repeated when held down.
- Moving by clicking on the mouse is jarring due to lack of animation
- Dwarfs move in a set path when placed (one tile left, two tiles up)

## TODO

The first "demoable release" will be to show at least one dwarf
mining out rooms on the layer.

Please see the TODO.org file for more information.

# How to build

You need to have the SDL2 libraries installed on your system. The best
way to do this is documented
[by the SDL2 crate](https://github.com/Rust-SDL2/rust-sdl2#user-content-requirements).

After that:
```
cargo build
```

# How to run
```
cargo run
```

## How to run the tests

cargo test

## Contributing

Suggestions, patches, pull requests, etc are very welcome.  There are
a few things you should keep in mind, though.

  - I will not always incorporate every change submitted.  Sometimes this
    will be a strange choice, but in my personal projects I'm often looking
    to try out weird ideas.  Please don't take it personally if I decide
    to go another way.  The code is free software specifically so that you
    can always incorporate your own ideas in a fork of the software.

  - I don't mind forks at all.  If you have an idea that you want to try out,
    or if you want to base your own game on the work I've started, you are
    heavily encouraged to do so.  I will cheer you on!  The only constraint
    is that the license is not changed.  The base rule is: "My fork is mine.
    Your fork is yours."

  - My goal is to build a project where everyone can feel safe contributing
    without worrying that the rules will change under them.  Technically I
    have the most power because I own the copyright for most of the code,
    however I wish to act in such a way that everyone can use the code
    with the same rights *and* restrictions -- an even playing field.

  - Before making *any* changes or suggestions, please read the LICENSE file.
    If you don't agree with the license, make sure that you do not distribute
    any changes to the code you are making.  I've written some reasoning behind
    my choice of the AGPL with this code at the bottom of this document, but
    I don't expect everyone to agree.  That's fine, but the use of this software
    is dependent upon the license.

  - All copyright for changes to this code belongs with their authors.  I'm
    happy to accept changes and you will own the copyright for that change.
    Over time we will have a mosaic of changes from a variety of authors
    and it will be impossible to say "This copyright belongs to me".  This is
    the best way I can think of to guarantee that I will *never* change the
    license (except for the "and later versions" exception in the license).

  - This project has no published code of conduct.  In keeping with the idea
    of "My fork is mine", the code of conduct on the stream, in discussions
    via email, etc, etc will be guided by my choices.  I will try my best to
    be accommodating because I would like people to enjoy contributing to
    the project.  However, if I fail your response should be to fork the
    project and show, through your actions, where I'm going wrong.

  - One of the goals of this project is for me to explore the idea of making
    money from a free software project.  If you are opposed to this, please
    do not contribute to the project.  Similarly, keep in mind that I am
    happy if you, in turn, make money from this project.  I will cheer you
    on!  Just keep in mind that you must follow the license.

To sum up, I don't want people to invest in something they later
regret.  The idea is that if you contribute, then your version of the
code is yours just as much as my version is mine.  Your ideas are just
as valid as my ideas and I want you to explore them to your hearts
content.  I would like you to feel the same way about what I'm doing.
The license imposes restrictions on what we can do with our code, so
make sure you are OK with that *before* starting on your journey.

## License

AGPL 3.0.  Please see the accompanying LICENSE.md file.

The DejaVuSerif.ttf font file is under this license:
https://dejavu-fonts.github.io/License.html
Eventually I'll find a better way to reference asset licenses.

Why AGPL?  My goal is to create a free software game I can get paid
for.  I think that copyleft licenses are good in that regard because
it forces all participants (including myself) to play by the same
rules.  I'm committed to writing only free software, but potential
competitors are not necessarily constrained by my choices.  I would
prefer not to compete against someone using my code that has more
options than I do.  From that perspective, copyleft licenses are ideal
for commercial endeavours.

You might be wondering, why free software in the first place.  Isn't
that making life difficult for myself?  Yes, it is.  However, I've
gained a lot from the free software I've received.  Not only has it
been easy on my pocket book, but I've been able to learn a huge amount
simply by having the freedom to inspect and work on the software.

There is something about a game that just invites exploration and
customisation.  In some ways it's the perfect candidate for software
freedom.  On the other hand, most people believe that game software
simply can not be sold unless the base source code is witheld from
players.  I can understand that point of view, but I'd love if someone
would find a way around it.

This project is as much about trying to find that way as it is about
trying to write a game.  I think it's pretty unlikely that I will
succeed, but I will have no chance of success if I don't try.  What's
the worst that can happen?
