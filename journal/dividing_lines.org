* Programming Journey

* Doing
** TODO Fix use of destination
** TODO Fix the tests
*** TODO Create the right side divider for GridPane
** TODO Divide all of the parts of the border into components
*** TODO Create a grid frame
**** DONE Write some tests
     CLOSED: [2019-12-03 Tue 09:36]
**** TODO Create a mesh
**** TODO Create a divider
     Convention: The upper left pane generates the dividers for the right and bottom of its pane
***** TODO Create the right side divider
***** TODO Create the bottom divider

* Plan
** Refactor Border class
*** TODO Divide all of the parts of the border into components
**** TODO Create a window frame
**** TODO Create a grid frame
**** TODO Create a status frame
**** TODO Create dividers between the frames
*** TODO Use the same build_mesh function to draw all aspects of the border

* Done
** DONE Made a plan
** DONE Move the shrinking of the rect to the constructor.  But the text is in the wrong place
*** DONE Make the border rectangle the actual rectangle that we want to draw
**** DONE Fix the text position
***** DONE Fix the tests
** DONE Made a plan to split the class into frames and dividers
** DONE Made a constructor for a GridPane
** DONE Refactor grid_size to use GridPane
